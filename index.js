var numberArr = [];
function themSo() {
    var input = document.getElementById("txt-number");

    // kiểm tra rỗng
    if (input.value == "") return;

    var number = input.value * 1;
    // reset value
    input.value = "";

    numberArr.push(number);
    document.getElementById("result").innerHTML = `👉 ${numberArr}`;
}
// bt1
function tinhTong() {
    var sum = 0;
    for (var i = 0; i < numberArr.length; i++) {
        if (numberArr[i] > 0) sum += numberArr[i];
    }
    document.getElementById("result1").innerHTML = `👉 ${sum}`;
}
// bt2
function demSoDuong() {
    var countPositive = 0;
    for (var i = 0; i < numberArr.length; i++) {
        if (numberArr[i] > 0) countPositive++;
    }
    document.getElementById("result2").innerHTML = `👉 ${countPositive}`;
}
// bt3
function timSoNhoNhat() {
    var minNumber = numberArr[0];
    for (var i = 0; i < numberArr.length; i++) {
        if (numberArr[i] < minNumber) minNumber = numberArr[i];
    }
    document.getElementById("result3").innerHTML = `👉 ${minNumber}`;
}
// bt4
function timSoDuongNhoNhat() {
    var newPositiveArr = [];
    for (var i = 0; i < numberArr.length; i++) {
        if (numberArr[i] > 0) newPositiveArr.push(numberArr[i]);
    }
    if (newPositiveArr.length == 0)
        return (document.getElementById(
            "result4"
        ).innerHTML = `👉 Không có số dương trong mảng`);

    var minPositiveNumber = newPositiveArr[0];
    for (var i = 0; i < newPositiveArr.length; i++) {
        if (newPositiveArr[i] < minPositiveNumber)
            minPositiveNumber = newPositiveArr[i];
    }

    document.getElementById(
        "result4"
    ).innerHTML = `👉Số dương nhỏ nhất: ${minPositiveNumber}`;
}
// bt5
function timSoChanCuoiCung() {
    var soChanCuoiCung = -1;
    for (var i = 0; i < numberArr.length; i++) {
        if (numberArr[i] % 2 == 0) soChanCuoiCung = numberArr[i];
    }
    if (soChanCuoiCung == -1)
        return (document.getElementById(
            "result5"
        ).innerHTML = `👉 Không có số chẵn`);
    document.getElementById(
        "result5"
    ).innerHTML = `👉Số chẵn cuối cùng: ${soChanCuoiCung}`;
}
// bt6
function doiCho() {
    var viTri_1 = document.getElementById("vitri-1").value;
    var viTri_2 = document.getElementById("vitri-2").value;
    var temp = numberArr[viTri_1];
    numberArr[viTri_1] = numberArr[viTri_2];
    numberArr[viTri_2] = temp;
    document.getElementById(
        "result6"
    ).innerHTML = `👉Mảng sau khi đổi: ${numberArr}`;
}
// bt7
function sapXep() {
    var temp;
    for (var i = 0; i < numberArr.length; i++) {
        for (var j = i + 1; j < numberArr.length; j++)
            if (numberArr[i] > numberArr[j]) {
                temp = numberArr[i];
                numberArr[i] = numberArr[j];
                numberArr[j] = temp;
            }
    }
    document.getElementById(
        "result7"
    ).innerHTML = `👉Mảng sau khi sắp xếp: ${numberArr}`;
}
// bt8
function kiemTraSNT(number) {
    var count = 0;
    if (number < 2) return false;
    for (var i = 2; i <= Math.sqrt(number); i++) {
        if (number % i == 0) count++;
    }
    if (count == 0) return true;
    return false;
}

function timSntDauTien() {
    var content = "";
    var count = 0;
    for (var i = 0; i < numberArr.length; i++) {
        if (kiemTraSNT(numberArr[i]) == true) {
            content = `👉 Số nguyên tố đầu tiên: ${numberArr[i]}`;
            count++;
            break;
        }
    }
    if (count == 0)
        return (document.getElementById(
            "result8"
        ).innerHTML = `👉 Mảng không có số nguyên tố`);
    document.getElementById("result8").innerHTML = content;
}
// bt9
var numberArr_9 = [];
function themSo_9() {
    var inputEl = document.getElementById("txt-number-9");
    if (inputEl.value == "") return;
    var number = inputEl.value * 1;
    numberArr_9.push(number);
    inputEl.value = "";
    document.getElementById("result9").innerHTML = `👉 ${numberArr_9}`;
}
function demSoNguyen() {
    var count = 0;
    for (var i = 0; i < numberArr_9.length; i++) {
        if (Number.isInteger(numberArr_9[i])) count++;
    }
    document.getElementById("result9-dem").innerHTML = `👉Số nguyên: ${count}`;
}
// bt10
function soSanh() {
    var content = "";
    var countPositive = 0;
    var countNegative = 0;
    for (var i = 0; i < numberArr.length; i++) {
        if (numberArr[i] > 0) countPositive++;
        if (numberArr[i] < 0) countNegative++;
    }
    if (countNegative > countPositive) {
        content = `👉Số âm > Số dương`;
    } else if (countNegative < countPositive) {
        content = `👉Số âm < Số dương`;
    } else {
        content = `👉Số âm = Số dương`;
    }
    document.getElementById("result10").innerHTML = content;
}
